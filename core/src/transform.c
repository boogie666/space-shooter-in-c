#include <core/transform.h>


void TransformModuleImport(ecs_world_t *world, int flags){

    ECS_MODULE(world, TransformModule);
    ECS_COMPONENT(world, Position);
    ECS_COMPONENT(world, Rotation);
    ECS_COMPONENT(world, Scale);

    ECS_EXPORT_COMPONENT(Position);
    ECS_EXPORT_COMPONENT(Rotation);
    ECS_EXPORT_COMPONENT(Scale);
}


