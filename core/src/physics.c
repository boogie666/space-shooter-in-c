#include <core/physics.h>
#include <math.h>

#define PI 3.1415927f
#define RAD_DEG (180.0f / PI) 

void PhysicsLoop(ecs_rows_t *rows){
  ECS_COLUMN(rows, Force, forces, 1);
  ECS_COLUMN(rows, Mass, masses, 2);
  ECS_COLUMN(rows, Velocity, velocities, 3);
  ECS_COLUMN(rows, Position, positions, 4);
  ECS_COLUMN(rows, Rotation, rotations, 5);
  float dt = rows->delta_time;
  
  for(int i = 0; i < rows->count;  i++){
    
    float invMass = 1.0f / masses[i].value;
    velocities[i].x += invMass * forces[i].x * dt;
    velocities[i].y += invMass * forces[i].y * dt;
  

    positions[i].x += velocities[i].x * dt;
    positions[i].y += velocities[i].y * dt;
    
    rotations[i].angle = atan2(velocities[i].y, velocities[i].x) * RAD_DEG;
    
    forces[i].x = 0;
    forces[i].y = 0;
  }
}

void PhysicsModuleImport(ecs_world_t *world, int flags){
    ECS_IMPORT(world, TransformModule, flags);

    ECS_MODULE(world, PhysicsModule);
    ECS_COMPONENT(world, Force);
    ECS_COMPONENT(world, Velocity);
    ECS_COMPONENT(world, Mass);
    
    ECS_TYPE(world, Body, Force, Mass, Velocity, Position);

    ECS_SYSTEM(world, PhysicsLoop, EcsOnUpdate, Force, Mass, Velocity, Position, Rotation, SYSTEM.EcsHidden);

    ECS_EXPORT_COMPONENT(Force);
    ECS_EXPORT_COMPONENT(Velocity);
    ECS_EXPORT_COMPONENT(Mass);
    ECS_EXPORT_ENTITY(PhysicsLoop);
}


