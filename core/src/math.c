#include <core/math.h>

float vec2_len(vec2d *v){
  float d2 = v->x * v->x + v->y * v->y;
  return sqrt(d2);
}

void vec2_cpy(vec2d *a, vec2d *b){
  a->x = b->x;
  a->y = b->y;
}

void vec2_sub_ptr(vec2d *a, vec2d *b){
  a->x -= b->x;
  a->y -= b->y;
}

vec2d vec2_sub(vec2d a, vec2d b){
  return (vec2d) { a.x - b.x, a.y - b.y };
}


void vec2_add_ptr(vec2d *a, vec2d *b){
  a->x += b->x;
  a->y += b->y;
}

vec2d vec2_add(vec2d a, vec2d b){
  return (vec2d) { a.x + b.x, a.y + b.y };
}

void vec2_nor(vec2d *v){
  float l = vec2_len(v);
  v->x /= l;
  v->y /= l;
}

void  vec2_scl(vec2d *v, float s){
  v->x *= s;
  v->y *= s;
}

void vec2_limit(vec2d *v, float limit){
  float len = vec2_len(v);
  if(len > limit){
    vec2_scl(v, limit / len);
  }

}
