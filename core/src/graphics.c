#include <core/graphics.h>

void RenderLoop(ecs_rows_t *rows){
  Window *wnd = ecs_column(rows, Window, 1);
  ecs_entity_t RenderImage = ecs_column_entity(rows, 2);
  SDL_Renderer *renderer = wnd->renderer;
  SDL_Event e;
  while(SDL_PollEvent(&e)){
    switch(e.type){
      case SDL_QUIT:
        ecs_quit(rows->world);
        break;
    }
  }

  SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
  SDL_RenderClear(renderer);
  ecs_run(rows->world, RenderImage, 0, wnd);
  SDL_RenderPresent(renderer);
}

void RenderImage(ecs_rows_t *rows){
  ECS_COLUMN(rows, Image, images, 1);
  ECS_COLUMN(rows, Position, positions, 2);
  ECS_COLUMN(rows, Rotation, rotations, 3);
  ECS_COLUMN(rows, Scale, scales, 4);
  Window *wnd = rows->param;
  SDL_Rect destRect;
  for(int i = 0; i < rows->count; i++){
    destRect.x = positions[i].x;
    destRect.y = positions[i].y;
    destRect.w = images[i].width * scales[i].x;
    destRect.h = images[i].height * scales[i].x;
    SDL_RenderCopyEx(wnd->renderer, images[i].texture, 
        NULL, &destRect, 
        rotations[i].angle,
        NULL, false);
  }

}


void GraphicsModuleImport(ecs_world_t *world, int flags){

    ECS_IMPORT(world, TransformModule, flags);
    ECS_MODULE(world, GraphicsModule);
    ECS_COMPONENT(world, Window);
    ECS_COMPONENT(world, Image);

    ECS_SYSTEM(world, RenderImage, EcsManual, Image, Position, Rotation, Scale, SYSTEM.EcsHidden);
    ECS_SYSTEM(world, RenderLoop, EcsOnStore, Window, .RenderImage, SYSTEM.EcsHidden);

    ECS_EXPORT_COMPONENT(Window);
    ECS_EXPORT_COMPONENT(Image);
    ECS_EXPORT_ENTITY(RenderLoop);
    ECS_EXPORT_ENTITY(RenderImage);
}


