#include <core.h>
#include <core/graphics.h>
#include <core/transform.h>
#include <core/physics.h>
#include <core/math.h>

typedef vec2d SeekTarget;

void SeekLoop(ecs_rows_t *rows){
  ECS_COLUMN(rows, SeekTarget, target, 1);
  ECS_COLUMN(rows, Velocity, vel, 2);
  ECS_COLUMN(rows, Position, pos, 3);
  ECS_COLUMN(rows, Force, f, 4);
  
  int x; 
  int y;

  SDL_GetMouseState(&x, &y);

  for(int i = 0; i < rows->count; i++){
    target[i].x = x;
    target[i].y = y;

    vec2d desired = vec2_sub(target[i], pos[i]);

    vec2_nor(&desired);
    vec2_scl(&desired, 400);
    
    vec2d steer = vec2_sub(desired, vel[i]);
    vec2_nor(&steer);
    vec2_scl(&steer, 100);

    vec2_add_ptr(&f[i], &steer);
    vec2_limit(&f[i], 50);
  }
}


int main(int argc, char *argv[]) {
  ecs_world_t *world = ecs_init_w_args(argc, argv);
  ECS_IMPORT(world, GraphicsModule, 0);
  ECS_IMPORT(world, TransformModule, 0);
  ECS_IMPORT(world, PhysicsModule, 0);
  
  ECS_COMPONENT(world, SeekTarget);
  
  ECS_SYSTEM(world, SeekLoop, EcsOnUpdate,  SeekTarget, Velocity, Position, Force);

  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
  SDL_Window *window = SDL_CreateWindow("Hello world", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);
  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  SDL_Texture *ship = IMG_LoadTexture(renderer, "images/ship.png");
  int w, h;
  SDL_QueryTexture(ship, NULL, NULL, &w, &h);
  
  ECS_TYPE(world, Sprite, Body, Image, Position, Rotation, Scale, SeekTarget);
  ECS_PREFAB(world, Ship, Sprite);
  ecs_set(world, Ship, SeekTarget, {400, 300});
  ecs_set(world, Ship, Mass, {1});
  ecs_set(world, Ship, Force, {0,0});
  ecs_set(world, Ship, Scale, {0.25, 0.25});
  ecs_set(world, Ship, Rotation, {0});
  ecs_set(world, Ship, Image, {.width=w, .height=h, .texture=ship});

  ecs_set_singleton(world, Window, {
      .window = window,
      .renderer = renderer,
  }); 
  


  for(int i = 0; i < 100; i++){ 
    ecs_entity_t e = ecs_new_instance(world, Ship, Sprite);
    ecs_set(world, e, Position, {rand() % 2000, rand() % 2000});
  }


  ecs_set_target_fps(world, 120);
  while (ecs_progress(world, 0));
  
  SDL_DestroyTexture(ship);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  return ecs_fini(world);

  
}
