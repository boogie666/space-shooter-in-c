#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <core.h>
#include <core/math.h>

typedef vec2d Position;
typedef vec2d Scale;

typedef struct Rotation{
  float angle;
} Rotation;

typedef struct TransformModule {
    ECS_DECLARE_COMPONENT(Position);
    ECS_DECLARE_COMPONENT(Rotation);
    ECS_DECLARE_COMPONENT(Scale);
} TransformModule;

void TransformModuleImport(ecs_world_t *world, int flags);

#define TransformModuleImportHandles(handles)\
    ECS_IMPORT_COMPONENT(handles, Position);\
    ECS_IMPORT_COMPONENT(handles, Rotation);\
    ECS_IMPORT_COMPONENT(handles, Scale);


#endif
