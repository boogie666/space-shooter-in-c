#ifndef PHYSICS_H
#define PHYSICS_H

#include <core.h>
#include <core/math.h>
#include <core/transform.h>

typedef vec2d Force;
typedef vec2d Velocity;
typedef struct Mass {
  float value;
} Mass;

void PhysicsLoop(ecs_rows_t *rows);

typedef struct PhysicsModule {
    ECS_DECLARE_ENTITY(PhysicsLoop);
    ECS_DECLARE_COMPONENT(Force);
    ECS_DECLARE_COMPONENT(Velocity);
    ECS_DECLARE_COMPONENT(Mass);
} PhysicsModule;

void PhysicsModuleImport(ecs_world_t *world, int flags);

#define PhysicsModuleImportHandles(handles)\
    ECS_IMPORT_ENTITY(handles, PhysicsLoop);\
    ECS_IMPORT_COMPONENT(handles, Force);\
    ECS_IMPORT_COMPONENT(handles, Velocity);\
    ECS_IMPORT_COMPONENT(handles, Mass);

#endif
