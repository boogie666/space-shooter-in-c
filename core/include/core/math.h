#ifndef CORE_MATH_H
#define  CORE_MATH_H
#include <math.h>

typedef struct vec2d {
  float x;
  float y;
} vec2d;

float vec2_len(vec2d *v);

void vec2d_limit(vec2d *v, float limit);

void vec2_cpy(vec2d *a, vec2d *b);

void vec2_sub_ptr(vec2d *a, vec2d *b);
vec2d vec2_sub(vec2d a, vec2d b);

void vec2_add_ptr(vec2d *a, vec2d *b);
vec2d vec2_add(vec2d a, vec2d b);

void vec2_nor(vec2d *v);
void vec2_limit(vec2d *v, float l);
void  vec2_scl(vec2d *v, float s);


#endif
