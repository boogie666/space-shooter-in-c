#ifndef GRAPHICS_H
#define GRAPHICS_H

#include<core.h>
#include<core/transform.h>

typedef struct Window {
  SDL_Window *window;
  SDL_Renderer *renderer;
} Window; 

typedef struct Image {
  int width;
  int height;
  SDL_Texture *texture;
} Image;

void RenderLoop(ecs_rows_t *rows);
void RenderImage(ecs_rows_t *rows);

typedef struct GraphicsModule {
    ECS_DECLARE_COMPONENT(Window);
    ECS_DECLARE_COMPONENT(Image);
    ECS_DECLARE_ENTITY(RenderImage);
    ECS_DECLARE_ENTITY(RenderLoop);
} GraphicsModule;

void GraphicsModuleImport(ecs_world_t *world, int flags);

#define GraphicsModuleImportHandles(handles)\
    ECS_IMPORT_COMPONENT(handles, Window);\
    ECS_IMPORT_COMPONENT(handles, Image);\
    ECS_IMPORT_ENTITY(handles, RenderImage);\
    ECS_IMPORT_ENTITY(handles, RenderLoop);


#endif


